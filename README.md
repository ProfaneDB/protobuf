# Protobuf definitions needed by ProfaneDB

In here are the Protobuf definitions both internally required by ProfaneDB (`storage.proto`),
and those used to interact with it (`options.proto` for schema definition, `db.proto` as gRPC interface).

## Latest build

The GitLab CI job takes care of compiling the definitions with `protoc`, using [profanedb/grpc](https://hub.docker.com/r/profanedb/grpc/) docker image.  
A trigger on DockerHub fires a new build of this repository whenever a new version of gRPC was released.

The latest build artifact is [here](https://gitlab.com/ProfaneDB/protobuf/-/jobs/artifacts/master/download?job=deploy).
